getStationData <- function(start_date, end_date, station_id,headers = headers,dpclim = "public/DPClim/v1/",timesleep=5,base="https://public-api.meteofrance.fr") {
    start_date <- as.Date(start_date)
    end_date <- as.Date(end_date)
    url <- modify_url(base,path = paste0(dpclim,"commande-station/infrahoraire-6m"))
    # Format dates in ISO8601 format
    formatted_start_date <- format(start_date, "%Y-%m-%dT00:00:00Z", tz = "GMT")
    formatted_end_date <- format(end_date, "%Y-%m-%dT00:00:00Z", tz = "GMT")

    parameters <- list("id-station" = station_id, "date-deb-periode" = formatted_start_date, "date-fin-periode" = formatted_end_date)

    # Create the complete URL with parameters
    url_with_params <- modify_url(url, query = parameters)

    # Add headers
    # Make the GET request
    response <- request_api(url_with_params, headers,code=202)
    if(response$status_code == 202){
        url <- modify_url(base,path = paste0(dpclim,"commande/fichier"))
        parameters <- list("id-cmde" = content(response)[[1]][[1]] )
        url_with_params <- modify_url(url, query = parameters)
        response <- request_api(url_with_params, headers,code=c(204,201))
        while(response$status_code == 204){
            print("file not finished yet, wait 5s")
            Sys.sleep(60)
            response <- request_api(url_with_params, headers,code=c(204,201))
        }
        if(response$status_code == 201){
            return(read.csv(text=content(response,as="text")[[1]],sep=";",header=T,dec = ","))
        }
    }
    print("Failed to retrieve data from the API. Why? not sure..")
	return(NULL)
}


