getStationInfo <- function(station, headers,infostat  = "information-station",dpclim = "public/DPClim/v1/") {
  # Assuming 'base' and 'dpclim' are defined externally; include 'infostat' parameter appropriately
  url <- modify_url(base, path = paste0(dpclim, infostat), query = list("id-station" = station))
  response <- GET(url,  headers)
  # Check if the request was successful
  if (http_status(response)$category == "Success") {  # Ensure 'success' is lowercase as typically returned by the http_status() function
    # Properly parsing the content as CSV by directly reading the response text
    data <- read.csv(text = content(response, as = "text"), sep = ";", header = TRUE)
    return(data)  
  } 
  stop(sprintf("Failed to get station info  from the API. Status code: %s", response$status_code))
}


